// ==UserScript==
// @name        Booru Hydrus Checker
// @description Mark media that already exists on hydrus.
// @version     1.1.1
// @namespace   relaxeaza/userscripts
// @grant       none
// @run-at      document-start
// @icon        https://i.imgur.com/pi2aL2k.jpg
// @include     https://gelbooru.com/index.php?page=post&s=list*
// @include     https://gelbooru.com//index.php?page=post&s=list*
// @include     https://danbooru.donmai.us/
// @include     https://danbooru.donmai.us/posts?*
// @include     https://safebooru.org/index.php?page=post&s=list*
// @downloadURL https://gitlab.com/relaxeaza/userscripts/raw/master/booru-hydrus-checker.user.js
// ==/UserScript==

const HYDRUS_HOST = "http://127.0.0.1:45869";
const HYDRUS_API_KEY =
  "146ab1723e31a2dca0348bc03576b8f7f0587d1201beb3e31a8ee0ebd85d0776";

let $style;
const booruHandlers = new Map();

async function init() {
  createStyles();

  if (booruHandlers.has(location.host)) {
    const permitted = await hydrusCheckPermission();
    if (permitted) {
      const handler = booruHandlers.get(location.host);
      handler && handler();
    } else {
      throw new Error("Missing hydrus permissions");
    }
  }
}

function addStyles(css) {
  $style.innerHTML += css.join("");
}

function createStyles() {
  $style = document.createElement("style");
  document.head.appendChild($style);

  addStyles([".rlx-hydrus-check {", "outline: 2px dotted #69c169;", "}"]);
}

async function requestHydrus(api, data = {}) {
  const requestUrl = new URL(api, HYDRUS_HOST);
  requestUrl.searchParams.set("Hydrus-Client-API-Access-Key", HYDRUS_API_KEY);
  Object.entries(data).forEach(([key, value]) =>
    requestUrl.searchParams.set(key, value)
  );
  const request = await fetch(requestUrl);
  return request.json();
}

async function hydrusUrlExists(url) {
  const result = await requestHydrus("/add_urls/get_url_files", {
    url: encodeURIComponent(url),
  });
  console.log(result);
  const [fileStatus] = result["url_file_statuses"];
  return fileStatus && fileStatus["status"] === 2;
}

async function hydrusCheckPermission() {
  const result = await requestHydrus("/verify_access_key");
  return result["basic_permissions"].some((permission) => permission === 0);
}

booruHandlers.set("danbooru.donmai.us", async function () {
  const $posts = document.querySelectorAll(".posts-container article");
  for (const $post of $posts) {
    const url = $post.querySelector("a").href;
    const exists = await hydrusUrlExists(url);
    exists && $post.classList.add("rlx-hydrus-check");
  }
});

booruHandlers.set("gelbooru.com", async function () {
  const $posts = document.querySelectorAll(".thumbnail-preview a");
  for (const $post of $posts) {
    const url = $post.querySelector("a").href;
    const exists = await hydrusUrlExists(url);
    exists && $post.children[0].classList.add("rlx-hydrus-check");
  }
});

booruHandlers.set("safebooru.org", async function () {
  const $posts = document.querySelectorAll("#post-list .thumb a");
  for (const $post of $posts) {
    const url = $post.querySelector("a").href;
    const exists = await hydrusUrlExists(url);
    exists && $post.children[0].classList.add("rlx-hydrus-check");
  }
});

document.addEventListener("DOMContentLoaded", init);
