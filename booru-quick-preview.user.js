// ==UserScript==
// @name        Danbooru Quick Preview
// @description Preview media without opening the post page.
// @namespace   relaxeaza/userscripts
// @version     2.2.2
// @grant       none
// @run-at      document-start
// @icon        https://i.imgur.com/pi2aL2k.jpg
// @include     https://danbooru.donmai.us/
// @include     https://danbooru.donmai.us/posts*
// @downloadURL https://gitlab.com/relaxeaza/userscripts/raw/master/booru-quick-preview.user.js
// ==/UserScript==

const cache = {};
let $overlay;
let $loading;
let $previewImg;
/** @type {HTMLVideoElement} */
let $previewVideo;
let $videoSource;
let eventsId = 0;
let currentId;
let volume = 1;

document.addEventListener("DOMContentLoaded", function () {
  setupHtml();
  setupStyles();
  setupEvents();
});

function setupEvents() {
  jQuery(document).on("danbooru:post-preview-updated", function (event, data) {
    attachPostEvents(data.id);
  });

  for (const $post of document.querySelectorAll("#posts article")) {
    attachPostEvents($post.dataset["id"], $post);
  }
}

function attachPostEvents(id, $post) {
  $post = $post || document.querySelector("#post_" + id);

  const $img = $post.querySelector("img");

  $img.removeAttribute("title");

  $img.addEventListener("mouseenter", async function () {
    showPreview();

    const eventId = eventsId++;
    currentId = eventId;

    if (!cache[id]) {
      const url = `/posts/${id}.json`;
      const headers = { headers: { "Content-Type": "application/json" } };
      const response = await fetch(url, headers);
      cache[id] = response.json();
    }

    if (currentId !== eventId) {
      return;
    }

    const data = await cache[id];
    const source = data.large_file_url;
    const format = source.split(".").pop();
    const video = format === "mp4" || format === "webm";

    if (video) {
      setVideo(source);

      document.body.addEventListener("wheel", volumeEventHandler, {
        passive: false,
      });
    } else {
      setImage(source);
    }
  });

  $img.addEventListener("mouseleave", function () {
    currentId = ++eventsId;
    hidePreview();
    document.body.removeEventListener("wheel", volumeEventHandler, {
      passive: false,
    });
  });
}

/**
 * @param {WheelEvent} event
 */
function volumeEventHandler(event) {
  event.stopPropagation();
  event.preventDefault();

  const increase = event.deltaY < 0;

  volume += increase ? 0.1 : -0.1;
  volume = Math.min(1, Math.max(0, volume));

  $previewVideo.volume = volume;
}

function setVideo(source) {
  $videoSource.src = source;
  $previewVideo.load();
  $previewVideo.style.display = "block";
}

function setImage(source) {
  $previewImg.src = source;
  $previewImg.style.display = "block";
}

function showPreview() {
  $overlay.style.display = "flex";
}

function hidePreview() {
  $overlay.style.display = "none";
  $previewImg.style.display = "none";
  $previewImg.src = "";
  $previewVideo.style.display = "none";
  $previewVideo.pause();
}

function setupHtml() {
  $overlay = document.createElement("div");
  $loading = document.createElement("span");
  $previewImg = document.createElement("img");
  $previewVideo = document.createElement("video");
  $videoSource = document.createElement("source");

  $previewVideo.setAttribute("autoplay", "");
  $previewVideo.setAttribute("loop", "");
  $previewVideo.appendChild($videoSource);
  $overlay.appendChild($loading);
  $overlay.appendChild($previewImg);
  $overlay.appendChild($previewVideo);
  document.body.appendChild($overlay);

  $previewVideo.volume = volume;
}

function setupStyles() {
  $overlay.style.position = "fixed";
  $overlay.style.display = "none";
  $overlay.style.placeContent = "center";
  $overlay.style.alignItems = "center";
  $overlay.style.top = "0px";
  $overlay.style.left = "0px";
  $overlay.style.width = "100%";
  $overlay.style.height = "100%";
  $overlay.style.fontSize = "30px";
  $overlay.style.fontWeight = "bold";
  $overlay.style.color = "#0095F9";
  $overlay.style.textShadow = "2px 2px 0px #000000";
  $overlay.style.pointerEvents = "none";
  $overlay.style.zIndex = "1000";

  $loading.innerText = "loading...";
  $loading.style.position = "absolute";
  $loading.style.zIndex = "1";

  $previewImg.style.maxWidth = "90%";
  $previewImg.style.maxHeight = "90%";
  $previewImg.style.width = "auto";
  $previewImg.style.height = "auto";
  $previewImg.style.zIndex = "2";
  $previewImg.style.pointerEvents = "none";
  $previewImg.style.display = "none";

  $previewVideo.style.maxWidth = "90%";
  $previewVideo.style.maxHeight = "90%";
  $previewVideo.style.width = "auto";
  $previewVideo.style.height = "auto";
  $previewVideo.style.zIndex = "2";
  $previewVideo.style.pointerEvents = "none";
  $previewVideo.style.display = "none";
}
