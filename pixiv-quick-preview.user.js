// ==UserScript==
// @name        Pixiv Quick Preview
// @description Preview media without opening the post page.
// @namespace   relaxeaza/userscripts
// @version     1.1.0
// @grant       none
// @run-at      document-start
// @icon        https://gitlab.com/relaxeaza/userscripts/raw/master/share/pixiv-quick-preview.logo.png
// @include     https://www.pixiv.net*
// @downloadURL https://gitlab.com/relaxeaza/userscripts/raw/master/booru-quick-preview.user.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/sizzle/2.3.5/sizzle.js
// ==/UserScript==

const postsSelector =
  'a[href^="/member_illust.php"]:not(.rlx):has(img), a[href^="/en/artworks/"]:not(.rlx):has(img)';
const rSource =
  /(?:img-master|custom-thumb)\/img(\/\d{4}\/(?:\d{2}\/){5})(\d+)_p0/;
const imgBase = "https://i.pximg.net/img-master";

let $overlay;
let $loading;
let $media;
let timeout;

document.addEventListener("DOMContentLoaded", function () {
  new MutationObserver(setupEvents).observe(document.body, {
    childList: true,
    subtree: true,
  });

  setupHtml();
  setupStyles();
  setupHistory();
  setupEvents();
});

function setupEvents() {
  for (const $post of Sizzle(postsSelector)) {
    $post.classList.add("rlx");

    $post.addEventListener("mouseenter", function () {
      timeout = setTimeout(function () {
        const $img = $post.querySelector("img");
        const video = !!$post.querySelector("svg circle");
        showPreview($img.src, video);
      }, 250);
    });

    $post.addEventListener("mouseleave", function () {
      clearTimeout(timeout);
      hidePreview();
    });
  }
}

function getSource(url) {
  const match = url.match(rSource);
  if (match && match[1]) {
    return `${imgBase}/img${match[1]}${match[2]}_p0_master1200.jpg`;
  }
  return false;
}

function showPreview(src, isVideo) {
  $media.src = isVideo ? src : getSource(src);
  $overlay.style.display = "flex";
}

function hidePreview() {
  $overlay.style.display = "none";
  $media.src = "";
}

function setupHtml() {
  $overlay = document.createElement("div");
  $loading = document.createElement("span");
  $loading.innerText = "loading...";
  $media = document.createElement("img");

  $overlay.appendChild($loading);
  $overlay.appendChild($media);
  document.body.appendChild($overlay);
}

function setupStyles() {
  $overlay.style.position = "fixed";
  $overlay.style.display = "none";
  $overlay.style.placeContent = "center";
  $overlay.style.alignItems = "center";
  $overlay.style.top = "0px";
  $overlay.style.left = "0px";
  $overlay.style.width = "100%";
  $overlay.style.height = "100%";
  $overlay.style.fontSize = "30px";
  $overlay.style.fontWeight = "bold";
  $overlay.style.color = "#0095F9";
  $overlay.style.textShadow = "2px 2px 0px #000000";
  $overlay.style.pointerEvents = "none";
  $overlay.style.zIndex = "1000";

  $loading.style.position = "absolute";
  $loading.style.zIndex = "1";

  $media.style.maxWidth = "90%";
  $media.style.maxHeight = "90%";
  $media.style.width = "auto";
  $media.style.height = "auto";
  $media.style.zIndex = "2";
  $media.style.pointerEvents = "none";
}

function setupHistory() {
  let previousState = window.history.state;

  setInterval(function () {
    if (previousState !== window.history.state) {
      previousState = window.history.state;
      hidePreview();
    }
  }, 333);
}
